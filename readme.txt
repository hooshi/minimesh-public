------------------------------------------------------------------------ 
----- minimesh -- Mesh editting codebase for CS524, Spring 2018, UBC --- 
------------------------------------------------------------------------ 

----------------------------------------
-----------Building the code ----------- 
----------------------------------------

Install cmake. Then open a CMD window if you are on windows, and a
bash window in Linux. Make sure that the cmake executable is on you
PATH environment variable.

Then change directory into the build-opt, or build-dbg folders
depending on whether you want to build the code in debug or release
mode. Then copy the relevant command from the cmake-examples.txt.

You can build the code via the command line, or within visual studio.
The needed commands for command line are also provided in
cmake-examples.txt.

Before running the executable, also build the target COPY_DLLS on
windows.

NOTE:

If you are using visual studio, place the file
debuggin/eigen/msvc/eigen.natvis in
\path\to\msvc\Community\Common7\Packages\Debugger\Visualizers. Where
\path\to\msvc is the location of MSVC on your hard drive. On my
machine, for example, it is C:\Program Files (x86)\Microsoft Visual
Studio\2017. This will help you see the eigen matrices easier in the
debugger.

If you are using GDB, follow the instruction at top of the file
debugger/eigen/gdb/printers.py.

----------------------------------------
--------------- Learning the API ------- 
----------------------------------------

Start with minimesh/cli/main.cpp, and then work your way through
mesh_connectivity.[ch]pp, mesh_io.[ch]pp.

----------------------------------------
--------------- Contents --------------- 
----------------------------------------

- minimesh/

  The actual C++ codebase, divided to three main part.

  - core/

    This is the core part of the library. It will be built into the
    library, minimeshcore.

    - util:
      Some basic utility code to do minor tasks.
    
    - mesh_connectivity.[ch]pp:
      A class to build and store half-edge data structure.

    - mesh_io.[ch]pp
      A class to read and write mesh file formats.

  - cli/

    Short for command-line interface. This folder has a main()
    function, and will be built into and executable which links
    against minimeshcore.

  - gui/

    This folder containts code for the gui.

- build-opt/, build-dbg/

  Empty folders where you can run cmake inside. One for the optimized build
  and the other for debug builds.

- cmake/

  CMake scripts.

- debugger/

  Tools for vewing Eigen matrices nicely in the debugger.

- third-party/

  Third party dependencies compiled for win64. (Will soon add versions
  built for UBC CS Linux machines).

- mesh/

  A few example meshes to test your algorithms on.

- .vscode/

  Visual studio code config files, in case you decide to use it.

- webpage/

  The webpage for the library.




