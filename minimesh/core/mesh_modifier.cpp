#include <minimesh/core/mesh_modifier.hpp>
#include <minimesh/core/util/assert.hpp>

namespace minimesh
{

//
// Given two vertices, this function return the index of the half-edge going from v0 to v1.
// Returns -1 if no half-edge exists between the two vertices.
//
int
Mesh_modifier::get_halfedge_between_vertices(const int v0, const int v1)
{
  // Get a ring iterator for v0
  Mesh_connectivity::Vertex_ring_iterator ring_iter = mesh().vertex_ring_at(v0);

  int answer = mesh().invalid_index;

  // Loop over all half-edges that end at v0.
  do
  {
    // Make sure that the half-edge does end at v0
    assert(ring_iter.half_edge().dest().index() == v0);

    // If the half-edge also starts and v1, then it's twin
    // goes from v0 to v1. This would be the half-edge that
    // we were looking for
    if(ring_iter.half_edge().origin().index() == v1)
    {
      answer = ring_iter.half_edge().twin().index();
    }
  } while(ring_iter.advance());

  if(answer != mesh().invalid_index)
  {
    assert(mesh().half_edge_at(answer).origin().index() == v0);
    assert(mesh().half_edge_at(answer).dest().index() == v1);
  }

  return answer;
}


bool
Mesh_modifier::flip_edge(const int he_index)
{
  //
  // Take a reference to all involved entities
  //

  // HALF-EDGES
  Mesh_connectivity::Half_edge_iterator he0 = mesh().half_edge_at(he_index);
  Mesh_connectivity::Half_edge_iterator he1 = he0.twin();

  // meshes on the boundary are not flippable
  if(he0.face().is_equal(mesh().hole()) || he1.face().is_equal(mesh().hole()))
  {
    return false;
  }

  Mesh_connectivity::Half_edge_iterator he2 = he0.next();
  Mesh_connectivity::Half_edge_iterator he3 = he2.next();
  Mesh_connectivity::Half_edge_iterator he4 = he1.next();
  Mesh_connectivity::Half_edge_iterator he5 = he4.next();

  // VERTICES
  Mesh_connectivity::Vertex_iterator v0 = he1.origin();
  Mesh_connectivity::Vertex_iterator v1 = he0.origin();
  Mesh_connectivity::Vertex_iterator v2 = he3.origin();
  Mesh_connectivity::Vertex_iterator v3 = he5.origin();

  // FACES
  Mesh_connectivity::Face_iterator f0 = he0.face();
  Mesh_connectivity::Face_iterator f1 = he1.face();

  //
  // Now modify the connectivity
  //

  // HALF-EDGES
  he0.data().next = he3.index();
  he0.data().prev = he4.index();
  he0.data().origin = v3.index();
  //
  he1.data().next = he5.index();
  he1.data().prev = he2.index();
  he1.data().origin = v2.index();
  //
  he2.data().next = he1.index();
  he2.data().prev = he5.index();
  he2.data().face = f1.index();
  //
  he3.data().next = he4.index();
  he3.data().prev = he0.index();
  //
  he4.data().next = he0.index();
  he4.data().prev = he3.index();
  he4.data().face = f0.index();
  //
  he5.data().next = he2.index();
  he5.data().prev = he1.index();

  // VERTICES
  v0.data().half_edge = he2.index();
  v1.data().half_edge = he4.index();
  v2.data().half_edge = he1.index();
  v3.data().half_edge = he0.index();

  // FACES
  f0.data().half_edge = he0.index();
  f1.data().half_edge = he1.index();


  // operation successful
  return true;
} // All done


bool
Mesh_modifier::collapse_edge(const int he_id)
{
#define undoable() return false
#define doable() return true

  //
  // First perform some elementary checks
  //

  // Check if half edge is on the boundary
  Mesh_connectivity::Half_edge_iterator he0 = mesh().half_edge_at(he_id);

  //
  // Cannot collapse edge on boundary
  //
  if(he0.face().is_equal(mesh().hole()) || he0.twin().face().is_equal(mesh().hole()))
  {
    undoable();
  }

  //
  // This code cannot collapse edge with vertex on boundary
  //
  auto is_vertex_on_boundary = [this](int vid) {
    Mesh_connectivity::Vertex_ring_iterator ring = mesh().vertex_ring_at(vid);
    return ring.reset_boundary();
  };
  if(is_vertex_on_boundary(he0.origin().index()) || is_vertex_on_boundary(he0.dest().index()))
  {
    undoable();
  }

  //
  // All checks have passed, so now perform the collapse
  //

  // Gather all entities
  Mesh_connectivity::Half_edge_iterator he1 = he0.twin();
  Mesh_connectivity::Half_edge_iterator he2 = he0.next();
  Mesh_connectivity::Half_edge_iterator he3 = he2.next();
  Mesh_connectivity::Half_edge_iterator he4 = he1.next();
  Mesh_connectivity::Half_edge_iterator he5 = he4.next();
  //
  Mesh_connectivity::Half_edge_iterator he6 = he5.twin();
  Mesh_connectivity::Half_edge_iterator he7 = he4.twin();
  Mesh_connectivity::Half_edge_iterator he8 = he3.twin();
  Mesh_connectivity::Half_edge_iterator he9 = he2.twin();
  //
  Mesh_connectivity::Face_iterator f0 = he0.face();
  Mesh_connectivity::Face_iterator f1 = he1.face();
  //
  Mesh_connectivity::Vertex_iterator v0 = he0.origin();
  Mesh_connectivity::Vertex_iterator v1 = he1.origin();
  Mesh_connectivity::Vertex_iterator v2 = he5.origin();
  Mesh_connectivity::Vertex_iterator v3 = he3.origin();

  std::vector<Mesh_connectivity::Half_edge_iterator> fan;
  {
    Mesh_connectivity::Vertex_ring_iterator ring;

    ring = mesh().vertex_ring_at(v1.index());
    do
    {
      if((ring.half_edge().index() != he9.index()) && //
          (ring.half_edge().index() != he0.index()) && //
          (ring.half_edge().index() != he5.index()))
        fan.push_back(ring.half_edge().twin());
    } while(ring.advance());

    ring = mesh().vertex_ring_at(v0.index());
    do
    {
      if((ring.half_edge().index() != he3.index()) && //
          (ring.half_edge().index() != he1.index()) && //
          (ring.half_edge().index() != he7.index()))
        fan.push_back(ring.half_edge().twin());
    } while(ring.advance());
  }

  //
  // Modify connectivity
  //

  // Half edges
  he0.deactivate();
  he2.deactivate();
  he3.deactivate();
  //
  he1.deactivate();
  he4.deactivate();
  he5.deactivate();
  //
  v1.deactivate();
  //
  f0.deactivate();
  f1.deactivate();


  he8.data().twin = he9.index();
  he9.data().twin = he8.index();
  he7.data().twin = he6.index();
  he6.data().twin = he7.index();

  for(int i = 0; i < (int)fan.size(); ++i)
  {
    assert(fan[i].is_active());
    fan[i].data().origin = v0.index();
  }
  he6.data().origin = v0.index();

  v0.data().half_edge = he8.index();
  v2.data().half_edge = he7.index();
  v3.data().half_edge = he9.index();

  doable();

#undef undoable
#undef doable
}


} // end of minimesh