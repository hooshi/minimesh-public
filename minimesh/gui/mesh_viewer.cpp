#include <Eigen/Geometry>
#include <Eigen/LU>

#include <minimesh/core/util/disable_irrelevant_warnings.hpp>
#include <minimesh/core/util/numbers.hpp>

#include <minimesh/gui/mesh_viewer.hpp>
#include <minimesh/gui/opengl_headers.hpp>

namespace minimesh
{

void
Mesh_viewer::initialize(const Eigen::AlignedBox<float, 3> & bounding_box)
{
  _view_quaternion = Eigen::Vector4f(0, 0, 0, 1);
  _aspect = 1.0;
  _has_lighting = 1;
  _draw_vertices = 0;
  _draw_edges = 1;
  _draw_faces = 1;
  _draw_axis = 1;
  _mouse_button = invalid_index;

  _light_dir0 = Eigen::Vector4f((float)sqrt(1. / 3), (float)sqrt(1. / 3), (float)sqrt(1. / 3), 0.0f);
  _light_dir1 = Eigen::Vector4f(-(float)sqrt(1. / 3), (float)sqrt(1. / 3), (float)sqrt(1. / 3), 0.0f);

  _background_color = Eigen::Vector4f(0.8f, 0.8f, 0.8f, 1.0f);
  _light_color = Eigen::Vector4f(0.9f, 0.9f, 0.9f, 0.0f);
  _specular_color = Eigen::Vector4f(0.2f, 0.2f, 0.2f, 0.0f);
  _amb_color = Eigen::Vector4f(0.5f, 0.5f, 0.5f, 0.0f);
  _black = Eigen::Vector4f(0.0f, 0.0f, 0.0f, 0.0f);
  //_default_surface_color = Eigen::Vector4f(0.3f, 0.3f, 0.5f, 1.0f); // purple
  _default_surface_color = Eigen::Vector4f(0.6f, 0.6f, 0.6f, 1.0f);   // gray
  _exponent = 128;

  glEnable(GL_DEPTH_TEST);
  glFrontFace(GL_CCW);
  glDepthFunc(GL_LEQUAL);
  glPointSize(2.0);
  glPolygonOffset(0.5, 0.0);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);
  glShadeModel(GL_SMOOTH);

  // The stencil buffer is used for selecting vertices
  glEnable(GL_STENCIL_TEST);
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  Eigen::Vector3f min, max;
  min = bounding_box.min();
  max = bounding_box.max();

  _obj_center = (min + max) / 2.0;
  _center_offset.setZero();
  _bbox_diameter = (max - min).norm();

  // assuming a 45 degree field of view, 3*radiu of a bounding sphere
  // is a good viewing distance
  _view_distance = 1.5f * _bbox_diameter;
  _near = _view_distance - 1.4999f * _bbox_diameter;
  _far = _view_distance + 4.5f * _bbox_diameter;

  _mouse_function = MOUSE_VIEW;
  _selected_vertex_mesh_buffer_id = invalid_index;

  // Lighting and shading settings
  _has_lighting = true;
  glLightfv(GL_LIGHT0, GL_DIFFUSE, _amb_color.data());
  glLightfv(GL_LIGHT0, GL_SPECULAR, _light_color.data());
  glLightfv(GL_LIGHT0, GL_AMBIENT, _amb_color.data());
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, _light_color.data());
  glLightfv(GL_LIGHT1, GL_SPECULAR, _black.data());
  glLightfv(GL_LIGHT1, GL_AMBIENT, _black.data());
  glEnable(GL_LIGHT1);
  glEnable(GL_LIGHTING);

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, _black.data());
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, _black.data());
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, _specular_color.data());
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, _exponent);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 1.0);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, _black.data());
}

void
Mesh_viewer::draw()
{
  // clear buffers
  // glClearColor(0.9f, 0.9f, 0.9f, 0.9f);
  glClearColor(_background_color[0], _background_color[1], _background_color[2], _background_color[3]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  // a generic perspective transform
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45, _aspect, _near, _far);
  // rotate geometry
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(0.0, 0.0, _view_distance, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

  // View offset controlled by arrow keys
  glTranslatef(-_center_offset[0], -_center_offset[1], -_center_offset[2]);

  // update light direction
  glLightfv(GL_LIGHT0, GL_POSITION, _light_dir0.data());
  glLightfv(GL_LIGHT1, GL_POSITION, _light_dir1.data());

  Eigen::AngleAxis<float> aa(get_view_quaternion());
  glRotatef(aa.angle() / minimesh::numbers::pi * 180., aa.axis().x(), aa.axis().y(), aa.axis().z());

  // move object center to origin
  glTranslatef(-_obj_center[0], -_obj_center[1], -_obj_center[2]);

  // Draw trimesh faces
  glStencilFunc(GL_ALWAYS, 1, ~(0u));

  if(_draw_faces)
  {

    Eigen::Vector3i vids;
    Eigen::Vector3f normal;
    Eigen::Vector3f vf0, vf1, vf2;
    Eigen::Vector4f vc0, vc1, vc2;

    if(_has_lighting)
    {
      glEnable(GL_LIGHTING);
    }
    else
    {
      glDisable(GL_LIGHTING);
    }

    // draw face geometry
    glEnable(GL_POLYGON_OFFSET_FILL);
    glBegin(GL_TRIANGLES);
    for(unsigned tid = 0; tid < get_mesh_buffer().tri_conn.cols(); ++tid)
    {
      vids = get_mesh_buffer().tri_conn.col(tid);
      if(_mesh_buffer.vertex_colors.cols())
      {
        vc0 = _mesh_buffer.vertex_colors.col(tid * 3 + 0);
        vc1 = _mesh_buffer.vertex_colors.col(tid * 3 + 1);
        vc2 = _mesh_buffer.vertex_colors.col(tid * 3 + 2);
      }
      else
      {
        vc0 = vc1 = vc2 = _default_surface_color;
      }
      vf0 = get_mesh_buffer().vertices.col(vids[0]);
      vf1 = get_mesh_buffer().vertices.col(vids[1]);
      vf2 = get_mesh_buffer().vertices.col(vids[2]);
      normal = (vf1 - vf0).cross(vf2 - vf0).normalized();
      glNormal3fv(normal.data());
      glColor4fv(vc0.data());
      glVertex3fv(vf0.data());
      glColor4fv(vc1.data());
      glVertex3fv(vf1.data());
      glColor4fv(vc2.data());
      glVertex3fv(vf2.data());
    }
    glEnd();
  }
  glDisable(GL_POLYGON_OFFSET_FILL);
  glDisable(GL_LIGHTING);

  // glStencilFunc(GL_ALWAYS, 0, ~(0u));

  // Draw trimesh vertices
  if(_draw_vertices)
  {
    glPointSize(4);
    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_POINTS);
    for(unsigned vid = 0; vid < get_mesh_buffer().vertices.cols(); ++vid)
    {
      Eigen::Vector3f vf = get_mesh_buffer().vertices.col(vid);
      glVertex3fv(vf.data());
    }
    glEnd();
  }

  // Draw the selected vertex
  if((get_selected_vertex() != invalid_index) && (_mouse_function == MOUSE_SELECT))
  {
    glPointSize(10);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_POINTS);
    Eigen::Vector3f vf = get_mesh_buffer().vertices.col(_selected_vertex_mesh_buffer_id);
    glVertex3fv(vf.data());
    glEnd();
  }

  if((_mouse_button == 0) && (_mouse_function == MOUSE_MOVE_VERTEX))
  {
    glLineWidth(3);
    glColor3f(0.5, 0.0, 0.0);
    glBegin(GL_LINES);
    glVertex3fv(_pull_current_pos.data());
    glVertex3fv(_pull_initial_pos.data());
    glEnd();
  }


  // Draw trimesh edges
  if(_draw_edges)
  {
    glLineWidth(1);
    glColor3f(0.0, 0.0, 0.0);
    glBegin(GL_LINES);
    for(unsigned eid = 0; eid < get_mesh_buffer().edge_conn.cols(); ++eid)
    {
      Eigen::VectorXi vids = get_mesh_buffer().edge_conn.col(eid);
      Eigen::Vector3f v0, v1;
      v0 = get_mesh_buffer().vertices.col(vids[0]);
      v1 = get_mesh_buffer().vertices.col(vids[1]);
      glVertex3fv(v0.data());
      glVertex3fv(v1.data());
    }
    glEnd();
  }

  if(_draw_axis)
  {
    glPushAttrib(GL_LINE_BIT);
    glLineWidth(4);

    float axisLength = 1.;
    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

    glBegin(GL_LINES);
    for(int i = 0; i < 3; i++)
    {
      float color[3] = {0, 0, 0};
      color[i] = 1.0;
      glColor3fv(color);

      float vertex[3] = {0, 0, 0};
      vertex[i] = axisLength;
      glVertex3fv(vertex);
      glVertex3f(0, 0, 0);
    }
    glEnd();

    glEnable(GL_LIGHTING);
    glPopAttrib();
  }

  glutSwapBuffers();
}

bool
Mesh_viewer::window_reshaped(const int w, const int h)
{
  _win_width = w;
  _win_height = h;
  glViewport(0, 0, w, h);
  _aspect = float(w) / h;
  return true;
}

bool
Mesh_viewer::mouse_pushed(const int button, const int state, const int x, const int y)
{
  bool should_redraw = false;

  if(state == GLUT_UP)
  {
    _mouse_button = invalid_index;
    if(button==0 && _mouse_function == MOUSE_MOVE_VERTEX) should_redraw=true;
  }
  else
  {
    switch(button)
    {
    case 4:
    {
      _view_distance += 0.2f * std::abs(_view_distance);
      if(_view_distance < 0.0)
        _view_distance = 0.0;
      _mouse_button = invalid_index;
      should_redraw = true;
      break;
    } // end of button 3
    case 3:
    {
      _view_distance -= 0.2f * std::abs(_view_distance);
      if(_view_distance < 0.0)
        _view_distance = 0.0;
      _mouse_button = invalid_index;
      should_redraw = true;
      break;
    } // end of button 2
    case 2:
    {
      record_mouse_pos(x,y,button);
      should_redraw = false;
    }
    case 0:
    {
      switch(_mouse_function)
      {
      case MOUSE_VIEW:
      {
        record_mouse_pos(x, y, button);
        select_vertex(x, y);
        should_redraw = false;
        break;
      }
      case MOUSE_SELECT:
      {
        record_mouse_pos(x, y, button);
        select_vertex(x, y);
        should_redraw = true;
        break;
      }
      case MOUSE_MOVE_VERTEX:
      {
        record_mouse_pos(x, y, button);
        Eigen::Vector3f click_global_pos;
        _pull_initial_pos = _pull_current_pos = unproject_given_depth(x,y,0);
        should_redraw = false;
        break;
      }
      default:;
      } // End of mouse funciton
    default:;
    } // End of button 0
    } // End of switch(button)
  } // all done with MOUSE CLICKED
  return should_redraw;
} // All done

bool
Mesh_viewer::mouse_moved(const int x, const int y)
{
  bool should_redraw = false;

  if(_mouse_button == 2)
  {
    _center_offset += Eigen::Vector3f(-(x - _xPos), +(y - _yPos), 0) / 500.0f * _bbox_diameter;
    record_mouse_pos(x, y, _mouse_button);
    should_redraw = true;
  }
  else if(_mouse_button == 0)
  {
    switch(_mouse_function)
    {
    case MOUSE_VIEW: // Rotate geometry
    {
      Eigen::AngleAxis<float> aa_y((x - _xPos) / 250.0f, Eigen::Vector3f(0, 1, 0));
      Eigen::AngleAxis<float> aa_x((y - _yPos) / 250.0f, Eigen::Vector3f(1, 0, 0));
      _view_quaternion = (aa_y * aa_x * get_view_quaternion()).coeffs();
      record_mouse_pos(x, y, _mouse_button);
      should_redraw = true;
      break;
    }
    case MOUSE_SELECT:
    {
      // do nothing
      break;
    }
    case MOUSE_MOVE_VERTEX:
    {
      _pull_current_pos = unproject_given_depth(x,y,0);
      should_redraw = true;
      break;
    }
    default:
      printf("Mouse mode %d is not implemented yet. \n", _mouse_function);
    } // End of switch(mouse funciton)
  } // End of key number 0

  return should_redraw;
}

bool
Mesh_viewer::keyboard_pressed(unsigned char c, int x, int y)
{
  bool should_redraw = false;

  switch(c)
  {
  case GLUT_KEY_LEFT:
    _center_offset += Eigen::Vector3f(-0.1, 0, 0);
    should_redraw = true;
    break;
  case GLUT_KEY_RIGHT:
    _center_offset += Eigen::Vector3f(0.1, 0, 0);
    should_redraw = true;
    break;
  case GLUT_KEY_UP:
    _center_offset += Eigen::Vector3f(0, 0.1, 0);
    should_redraw = true;
    break;
  case GLUT_KEY_DOWN:
    _center_offset += Eigen::Vector3f(0, -0.1, 0);
    should_redraw = true;
    break;
  }

  return should_redraw;
}


int
Mesh_viewer::get_selected_vertex()
{
  if(_mouse_function == MOUSE_SELECT)
  {
    if(_selected_vertex_mesh_buffer_id == invalid_index)
      return invalid_index;
    else
      return _mesh_buffer.vertex_ids[_selected_vertex_mesh_buffer_id];
  }
  else
  {
    return invalid_index;
  }
}

Eigen::Quaternionf
Mesh_viewer::get_view_quaternion()
{
  return Eigen::Quaternionf(_view_quaternion.data());
}

void
Mesh_viewer::record_mouse_pos(int clickx, int clicky, int button)
{
  _xPos = clickx;
  _yPos = clicky;
  _mouse_button = button;
}

Eigen::Vector3f
Mesh_viewer::unproject_given_depth(int clickx, int clicky, float depth)
{
  Eigen::Matrix4d model_view_matrix;
  Eigen::Matrix4d projection_matrix;
  std::array<int, 4> view_port;
  glGetDoublev(GL_MODELVIEW_MATRIX, model_view_matrix.data());
  glGetDoublev(GL_PROJECTION_MATRIX, projection_matrix.data());
  glGetIntegerv(GL_VIEWPORT, view_port.data());

  int window_x = clickx;
  int window_y = view_port[3] - 1 - clicky;

  Eigen::Vector3d world_xyzd;
  gluUnProject(window_x,
      window_y,
      depth,
      model_view_matrix.data(),
      projection_matrix.data(),
      view_port.data(),
      &world_xyzd.x(),
      &world_xyzd.y(),
      &world_xyzd.z());
  return world_xyzd.cast<float>();
}

void
Mesh_viewer::unproject_find_depth(int clickx, int clicky, bool & is_mesh_clicked, Eigen::Vector3f & world_xyzf)
{
  Eigen::Matrix4d model_view_matrix;
  Eigen::Matrix4d projection_matrix;
  std::array<int, 4> view_port;
  glGetDoublev(GL_MODELVIEW_MATRIX, model_view_matrix.data());
  glGetDoublev(GL_PROJECTION_MATRIX, projection_matrix.data());
  glGetIntegerv(GL_VIEWPORT, view_port.data());

  int window_x = clickx;
  int window_y = view_port[3] - 1 - clicky;

  GLubyte stencil_value;
  glReadPixels(window_x, window_y, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &stencil_value);

  float depth_value;
  glReadPixels(window_x, window_y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth_value);


  Eigen::Vector3d world_xyzd;
  gluUnProject(window_x,
      window_y,
      depth_value,
      model_view_matrix.data(),
      projection_matrix.data(),
      view_port.data(),
      &world_xyzd.x(),
      &world_xyzd.y(),
      &world_xyzd.z());
  world_xyzf = world_xyzd.cast<float>();

  if(stencil_value)
  {
    is_mesh_clicked = true;
  }
  else
  {
    is_mesh_clicked = false;
  }
}

void
Mesh_viewer::select_vertex(int clickx, int clicky)
{
  Eigen::Vector3f world_xyzf;
  bool is_mesh_clicked;
  unproject_find_depth(clickx, clicky, is_mesh_clicked, world_xyzf);

  // Brute force search
  if(is_mesh_clicked)
  {
    _selected_vertex_mesh_buffer_id = invalid_index;
    float closest_dist = 1e10;
    for(int i = 0; i < (int)_mesh_buffer.vertices.cols(); ++i)
    {
      const float current_dist = (_mesh_buffer.vertices.col(i) - world_xyzf).squaredNorm();
      if(current_dist < closest_dist)
      {
        _selected_vertex_mesh_buffer_id = i;
        closest_dist = current_dist;
      }
    } // End of search
    // printf("Clicked on vertex: %d \n", get_selected_vertex());
  } // end of click on non-void
  else
  {
    // printf("Clicked on VOID, stencil value %d \n", (int)stencil_value);
  }
}

} // End of minimesh

#include <minimesh/core/util/enable_irrelevant_warnings.hpp>
