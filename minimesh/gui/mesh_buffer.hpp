#ifndef MINIMESH_VIEW_DATA_IS_INCLUDED
#define MINIMESH_VIEW_DATA_IS_INCLUDED

#include <Eigen/Core>

#include <minimesh/core/mesh_connectivity.hpp>

namespace minimesh
{


// An interim between a mesh and the viewer. To view any mesh, it should be converted to 
// a mesh buffer first.
class Mesh_buffer
{
public:
	// Rebuild the mesh buffer from a mesh.
	// It needs both the mesh and a defragmentation map for the mesh.
	void rebuild(Mesh_connectivity&, Mesh_connectivity::Defragmentation_maps &defrag);

	// Color the vertices of the mesh. Size of colors should be 4[RGBA] * n_active_vertices().
	// call this after rebuild.
	void set_vertex_colors(const Eigen::Matrix4Xf& colors);

	// Color the faces of the mesh. Size of colors should be 4[RGBA] * n_active_faces().
	// call this after rebuild.
	void set_face_colors(const Eigen::Matrix4Xf& colors);

private:
  // The vertices of the mesh
  Eigen::Matrix3Xf vertices;

  // Ids of vertices in the actual mesh (not defragmented)
  Eigen::VectorXi vertex_ids;

  // The connectivity for every triangle
  Eigen::Matrix3Xi tri_conn;

  // The connectivity for every edge
  Eigen::Matrix2Xi edge_conn;

  Eigen::Matrix4Xf vertex_colors;
  
  friend class Mesh_viewer;

};


} // end of minimesh


#endif /*MINIMESH_OPENGL_VIEWER_IS_INCLUDED*/
