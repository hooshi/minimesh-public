// From standard library
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

// eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// core
#include <minimesh/core/mesh_connectivity.hpp>
#include <minimesh/core/mesh_io.hpp>
#include <minimesh/core/util/foldertools.hpp>
#include <minimesh/core/util/numbers.hpp>

// gui
#include <minimesh/gui/mesh_viewer.hpp>
#include <minimesh/gui/opengl_headers.hpp>


using namespace minimesh;

// ======================================================
// Global variables
// ======================================================
namespace globalvars
{
Mesh_viewer viewer;
Mesh_connectivity mesh;
//
int glut_main_window_id;
//
GLUI * glui;
GLUI_Panel * panel_view;
GLUI_Panel * panel_mouse_func;
GLUI_RadioGroup * radio_group_mouse_func;
//
GLUI_Button * button_subdivide;
//
int num_entities_to_simplify;
GLUI_Spinner * spinner_simplify;
GLUI_Button * button_simplify;
}


// ======================================================
//              FREEGLUT CALL BACKS
// ======================================================
namespace freeglutcallback
{


void
draw()
{
  globalvars::viewer.draw();
}

void
window_reshaped(int w, int h)
{
  bool should_redraw = false;
  should_redraw = should_redraw || globalvars::viewer.window_reshaped(w, h);

  if(should_redraw)
    glutPostRedisplay();
}


void
keyboard_pressed(unsigned char c, int x, int y)
{
  bool should_redraw = false;
  should_redraw = should_redraw || globalvars::viewer.keyboard_pressed(c, x, y);

  if(should_redraw)
    glutPostRedisplay();
}

void
keyboard_arrows_pressed(int c, int x, int y)
{
  bool should_redraw = false;
  should_redraw = should_redraw || globalvars::viewer.keyboard_pressed(c, x, y);

  if(should_redraw)
    glutPostRedisplay();
}


void
mouse_pushed(int button, int state, int x, int y)
{
  bool should_redraw = false;
  should_redraw = should_redraw || globalvars::viewer.mouse_pushed(button, state, x, y);

  if(should_redraw)
    glutPostRedisplay();
}

void
mouse_moved(int x, int y)
{
  bool should_redraw = false;
  should_redraw = should_redraw || globalvars::viewer.mouse_moved(x, y);

  if(should_redraw)
    glutPostRedisplay();
}

void
subdivide_pressed(int)
{
  printf("Subdivide button was pressed \n");
}

void
simplify_pressed(int)
{
  printf("Simplify button was pressed to remove %d entities \n", globalvars::num_entities_to_simplify);
}

}

int
main(int argc, char * argv[])
{
  // Remember current folder
  foldertools::pushd();

  // If no command line argument is specified, load a hardcoded mesh.
  // Useful when debugging with visual studio.
  // Change the hardcoded address to your needs.
  if(argc == 1)
  {
    foldertools::makeandsetdir("D:/cartel-ii-public/mesh/");
    Mesh_io(globalvars::mesh).read_auto("cow_head.obj");
  }
  else // otherwise use the address specified in the command line
  {
    Mesh_io(globalvars::mesh).read_auto(argv[1]);
  }

  // Initialize GLUT window
  glutInit(&argc, argv);
  glutInitWindowSize(800, 600);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_DOUBLE);
  globalvars::glut_main_window_id = glutCreateWindow("Mesh Viewer");

  // Initialize GLUI window for buttons and ...
  globalvars::glui = GLUI_Master.create_glui("Controls");
  globalvars::glui->set_main_gfx_window(globalvars::glut_main_window_id);

  // Register callbacks
  glutDisplayFunc(freeglutcallback::draw);
  GLUI_Master.set_glutReshapeFunc(freeglutcallback::window_reshaped);
  GLUI_Master.set_glutKeyboardFunc(freeglutcallback::keyboard_pressed);
  GLUI_Master.set_glutSpecialFunc(freeglutcallback::keyboard_arrows_pressed);
  GLUI_Master.set_glutMouseFunc(freeglutcallback::mouse_pushed);
  glutMotionFunc(freeglutcallback::mouse_moved);
  GLUI_Master.set_glutIdleFunc(NULL);

  // Initialize the viewer (it needs the bounding box of the mesh)
  Eigen::AlignedBox3f bbox;
  for(int v = 0; v < globalvars::mesh.n_total_vertices(); ++v)
  {
    Mesh_connectivity::Vertex_iterator vertex = globalvars::mesh.vertex_at(v);
    if(vertex.is_active())
    {
      bbox.extend(vertex.xyz().cast<float>());
    }
  }
  globalvars::viewer.initialize(bbox);

  // Load the mesh in the viewer
  {
    Mesh_connectivity::Defragmentation_maps defrag;
    globalvars::mesh.compute_defragmention_maps(defrag);
    globalvars::viewer.get_mesh_buffer().rebuild(globalvars::mesh, defrag);
  }

  //
  // Add radio buttons to see which mesh components to view
  // Please view GLUI's user manual to learn more.
  //
  globalvars::panel_view = globalvars::glui->add_panel("View mesh components");
  globalvars::glui->add_checkbox_to_panel(
      globalvars::panel_view, "Show vertices", &globalvars::viewer.get_draw_vertices());
  globalvars::glui->add_checkbox_to_panel(globalvars::panel_view, "Show edges", &globalvars::viewer.get_draw_edges());
  globalvars::glui->add_checkbox_to_panel(globalvars::panel_view, "Show faces", &globalvars::viewer.get_draw_faces());
  globalvars::glui->add_checkbox_to_panel(globalvars::panel_view, "Show axis", &globalvars::viewer.get_draw_axis());
  globalvars::glui->add_checkbox_to_panel(
      globalvars::panel_view, "Show lighting", &globalvars::viewer.get_has_lighting());

  //
  // Add radio buttons to determine mouse left click functionality
  //
  globalvars::panel_mouse_func = globalvars::glui->add_panel("Mouse functionality");
  globalvars::radio_group_mouse_func =
      globalvars::glui->add_radiogroup_to_panel(globalvars::panel_mouse_func, &globalvars::viewer.get_mouse_function());
  for(int i = 0; i < Mesh_viewer::MOUSE_INVALID; ++i)
  {
    if(i == Mesh_viewer::MOUSE_VIEW)
      globalvars::glui->add_radiobutton_to_group(globalvars::radio_group_mouse_func, "Pan and zoom");
    if(i == Mesh_viewer::MOUSE_SELECT)
      globalvars::glui->add_radiobutton_to_group(globalvars::radio_group_mouse_func, "Select vertex");
    if(i == Mesh_viewer::MOUSE_MOVE_VERTEX)
      globalvars::glui->add_radiobutton_to_group(globalvars::radio_group_mouse_func, "Move vertex");
  }

  //
  // Add subdivide button
  //
  globalvars::button_subdivide =
      globalvars::glui->add_button("Subdivide Loop", -1, freeglutcallback::subdivide_pressed);
  globalvars::button_subdivide->set_w(200);

  //
  // Add simplify button and a spinner to read how many entities to remove
  //
  globalvars::num_entities_to_simplify = 0;
  globalvars::spinner_simplify = globalvars::glui->add_spinner(
	  "# of entities to simplify", GLUI_SPINNER_INT, &globalvars::num_entities_to_simplify);
  globalvars::spinner_simplify->set_alignment(GLUI_ALIGN_CENTER);
  globalvars::spinner_simplify->set_w(300);
  //
  globalvars::button_simplify = globalvars::glui->add_button("Simplify", -1, freeglutcallback::simplify_pressed);
  globalvars::button_simplify->set_w(200);

  // Sync all glui variables
  globalvars::glui->sync_live();

  // Start main loop
  glutMainLoop();

  // revert back to initial folder
  foldertools::popd();

  return 0;
}
