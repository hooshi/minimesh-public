#ifndef MINIMESH_OPENGL_VIEWER_IS_INCLUDED
#define MINIMESH_OPENGL_VIEWER_IS_INCLUDED

#include <vector>

#include <Eigen/Core>

#include <minimesh/gui/mesh_buffer.hpp>


namespace minimesh
{


// A very simple class to manage operations related to viewing and operating with
// a mesh.
class Mesh_viewer
{
public:
  // Initialize the viewer. bounding_box should be the bounding box of the mesh.
  void initialize(const Eigen::AlignedBox<float, 3> & bounding_box);

  // Draw() function. Call this within your freeglut draw function.
  void draw();

  // Call this within your freeglut window reshape function.
  // return true, if the scene has to be re-rendered, false otherwise
  bool window_reshaped(const int w, const int h);

  // Call this within your freeglut mouse pushed function.
  // return true, if the scene has to be re-rendered, false otherwise
  bool mouse_pushed(int button, const int state, const int x, const int y);

  // Call this within your freeglut mouse moved function.
  // return true, if the scene has to be re-rendered, false otherwise
  bool mouse_moved(const int x, const int y);

  // Call this within your freeglut keyboard pressed function.
  // return true, if the scene has to be re-rendered, false otherwise
  bool keyboard_pressed(unsigned char c, int x, int y);

  // Returns the mesh_buffer object associated with the viewer.
  // If the mesh changes, you should call get_mesh_buffer().rebuild(mesh)
  Mesh_buffer & get_mesh_buffer() {return _mesh_buffer;}

  //
  // What the mouse does.
  //
  enum  Mouse_function : int
  {
    MOUSE_VIEW = 0,
    MOUSE_SELECT,
    MOUSE_MOVE_VERTEX,
    MOUSE_INVALID,
  };

  constexpr static int invalid_index = -1; 

  // return functionality of the mouse.
  int& get_mouse_function() { return _mouse_function;}

  // Return the index of the selected vertex. Returns the invalid_index otherwise.
  int get_selected_vertex();

  //
  // Allow changing stuff 
  //
  int& get_has_lighting(){return _has_lighting;}
  int& get_draw_vertices(){return _draw_vertices;}
  int& get_draw_edges(){return _draw_edges;}
  int& get_draw_faces(){return _draw_faces;}
  int& get_draw_axis(){return _draw_axis;}
  Eigen::Quaternionf get_view_quaternion();


private:
  Mesh_buffer _mesh_buffer;

  int _has_lighting;
  int _draw_vertices;
  int _draw_edges;
  int _draw_faces;
  int _draw_axis;

  float _near;
  float _far;
  Eigen::Vector4f _view_quaternion;
  float _aspect;
  int _xPos;
  int _yPos;
  int _mouse_button;
  float _bbox_diameter;

  int _win_width;
  int _win_height;
  Eigen::Vector3f _obj_center;
  Eigen::Vector3f _center_offset;
  float _view_distance;

  Eigen::Vector4f _light_dir0;
  Eigen::Vector4f _light_dir1;
  Eigen::Vector4f _light_color;
  Eigen::Vector4f _specular_color;
  Eigen::Vector4f _amb_color;
  Eigen::Vector4f _black;
  Eigen::Vector4f _default_surface_color;
  Eigen::Vector4f _background_color;
  float _exponent;

  int _mouse_function;
  int _selected_vertex_mesh_buffer_id;
  Eigen::Vector3f _pull_current_pos;
  Eigen::Vector3f _pull_initial_pos;

  Eigen::Vector3f unproject_given_depth(int clickx, int clicky, float depth);
  void unproject_find_depth(int clickx, int clicky, bool &is_mesh_clicked, Eigen::Vector3f& global_pos);
  void select_vertex(int clickx, int clicky);
  void record_mouse_pos(int clickx, int clicky, int button);
};


} // end of minimesh


#endif /*MINIMESH_OPENGL_VIEWER_IS_INCLUDED*/
