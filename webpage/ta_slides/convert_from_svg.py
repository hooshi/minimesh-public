import os

# INKSCAPE = 'inkscape'
INKSCAPE = '"C:/Program Files/Inkscape/inkscape.exe"'
CONVERT = '{inkscape} -z {inf} -e {outf}'

FILES = [
    'half-edge',
    'edge-split',
    'topo-subdiv'
]

TO_PDF = 0
TO_PNG = 1

if TO_PNG:
    for fil in FILES:
        command = CONVERT.format(inkscape=INKSCAPE,inf=fil+'.svg',outf=fil+'.png')
        print(command)
        os.system(command)

if TO_PDF:
    for fil in FILES:
        command = CONVERT.format(inkscape=INKSCAPE,inf=fil+'.svg',outf=fil+'.pdf')
        print(command)
        os.system(command)
